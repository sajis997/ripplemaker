#include <QtGui/QOpenGLContext>
#include <QtQuick/QQuickWindow>
#include <QTimer>

#include "RippleScene.h"
#include "RippleSceneItem.h"

RippleSceneItem::RippleSceneItem()
    : mRippleScene(0)
{
    mRippleScene = new RippleScene();

    mTime.start();

    mTimer = new QTimer(this);
    mTimer->start();

    connect(this,SIGNAL(windowChanged(QQuickWindow*)),this,SLOT(handleWindowChanged(QQuickWindow*)),Qt::DirectConnection);
}

void RippleSceneItem::handleWindowChanged(QQuickWindow *win)
{
    if(win)
    {
        QSurfaceFormat f = win->format();
        f.setMajorVersion(4);
        f.setMinorVersion(3);
        f.setSamples(4);
        f.setStencilBufferSize(8);
        f.setProfile(QSurfaceFormat::CompatibilityProfile);

        win->setFormat(f);
        win->setClearBeforeRendering(false);

        connect(win,SIGNAL(beforeSynchronizing()),this,SLOT(sync()),Qt::DirectConnection);
        connect(win,SIGNAL(sceneGraphInvalidated()),this,SLOT(cleanup()),Qt::DirectConnection);
        connect(win,SIGNAL(beforeRendering()),mRippleScene,SLOT(paint()),Qt::DirectConnection);
    }
}

void RippleSceneItem::updateTime()
{
    float time = mTime.elapsed() * 0.001;

    mRippleScene->update(time);

    window()->update();
}

void RippleSceneItem::sync()
{
    if(mRippleScene)
    {
        connect(mTimer,SIGNAL(timeout()),this,SLOT(updateTime()),Qt::DirectConnection);

        mRippleScene->setViewportSize(window()->width() * window()->devicePixelRatio(),
                                      window()->height() * window()->devicePixelRatio());
    }
}

void RippleSceneItem::cleanup()
{
    if(mRippleScene)
    {
        delete mRippleScene;
        mRippleScene = 0;
    }
}

double RippleSceneItem::rX() const
{
    return mRippleScene->rX();
}

double RippleSceneItem::rY() const
{
    return mRippleScene->rY();
}

double RippleSceneItem::distance() const
{
    return mRippleScene->distance();
}

void RippleSceneItem::setRX(double rx)
{
    if(rx == mRippleScene->rX())
        return;

    mRippleScene->setRX(rx);

    emit rXChanged();
}


void RippleSceneItem::setRY(double ry)
{
    if(ry == mRippleScene->rY())
        return;

    mRippleScene->setRY(ry);

    emit rYChanged();
}


void RippleSceneItem::setDistance(double distance)
{
    if(distance == mRippleScene->distance())
        return;

    mRippleScene->setDistance(distance);

    emit distanceChanged();
}
