include ( common/common.pri )

TEMPLATE = app

INCLUDEPATH += common

HEADERS += \
    RippleScene.h \
    RippleSceneItem.h

SOURCES += \
    RippleScene.cpp \
    main.cpp \
    RippleSceneItem.cpp

OTHER_FILES += shaders/ripple.vert \
               shaders/ripple.frag

RESOURCES += \
    RippleMaker.qrc
