import QtQuick 2.2
import QtQuick.Controls 1.1
import RippleScene 1.0

Item
{
    id:root
    width: 512
    height: 512

    RippleSceneItem
    {
        id: ripple
    }

    MouseArea
    {
        anchors.fill: root
        //enable the mouse area
        enabled: true
        acceptedButtons: Qt.LeftButton | Qt.MiddleButton

        //more custom property for the mouse area
        property int oldMouseX: 0
        property int oldMouseY: 0

        property int currentMouseX: 0
        property int currentMouseY: 0

        onPressed:
        {
            oldMouseX = mouse.x
            oldMouseY = mouse.y
        }


        onPositionChanged:
        {

            //get the current mouse position
            currentMouseX = mouse.x
            currentMouseY = mouse.y

            if(mouse.buttons & Qt.LeftButton)
            {
                //update the rotation parameter for the scene item
                ripple.rY = ripple.rY + (mouse.x - oldMouseX)/5.0
                ripple.rX = ripple.rX + (mouse.y - oldMouseY)/5.0

            }
            else if(mouse.buttons & Qt.MiddleButton)
            {
                ripple.distance = ripple.distance * (1 + (currentMouseY - oldMouseY)/160.0)
            }

            //store the current mouse position as the old mouse position
            oldMouseX = currentMouseX
            oldMouseY = currentMouseY
        }
    }
}
