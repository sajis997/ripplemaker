#include <QTimer>

#include "RippleScene.h"
#include "GLSLShader.h"

RippleScene::RippleScene(QObject *parent)
    : AbstractScene(parent),
      mInitialized(false),
      mRippleShader(0),
      mCurrentTime(0.0f),
      mRx(25),
      mRy(-40),
      mDist(-7),
      NUM_X(40),
      NUM_Z(40),
      SIZE_X(4),
      SIZE_Z(4),
      HALF_SIZE_X(SIZE_X/4.0),
      HALF_SIZE_Z(SIZE_Z/4.0),
      SPEED(2),
      TOTAL_INDICES(NUM_X * NUM_Z * 2 * 3)
{
    mVertices.resize((NUM_X+1)*(NUM_Z+1));
    mIndices.resize(TOTAL_INDICES);

    mModelViewMatrix = glm::mat4(1.0f);
    mProjectionMatrix = glm::mat4(1.0f);
}

RippleScene::~RippleScene()
{
    if(mRippleShader)
    {
        mRippleShader->DeleteShaderProgram();
        delete mRippleShader;
        mRippleShader = NULL;
    }

    mVertices.clear();
    mIndices.clear();

    //destroy the vao and vbo
    glDeleteBuffers(1,&mVboVerticesID);
    glDeleteBuffers(1,&mVboIndicesID);
    glDeleteVertexArrays(1,&mVaoID);
}



void RippleScene::initialise()
{
    if(mInitialized)
        return;

    if(!initializeOpenGLFunctions())
    {
        std::cerr << "Modern OpenGL Functions could not be initialized." << std::endl;
        std::exit(EXIT_FAILURE);
    }

    GL_CHECK_ERRORS;

    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

    //load the shaders
    loadShaders();

    GL_CHECK_ERRORS;

    //setup plane vertices and geometry
    int count = 0;
    int i=0, j=0;

    for( j=0;j<=NUM_Z;j++)
    {
        for( i=0;i<=NUM_X;i++)
        {
            mVertices[count++] = glm::vec3( ((float(i)/(NUM_X-1)) *2-1)* HALF_SIZE_X, 0, ((float(j)/(NUM_Z-1))*2-1)*HALF_SIZE_Z);
        }
    }

    //fill plane indices array
    GLushort* id = &mIndices[0];

    for (i = 0; i < NUM_Z; i++)
    {
        for (j = 0; j < NUM_X; j++)
        {
            int i0 = i * (NUM_X+1) + j;
            int i1 = i0 + 1;
            int i2 = i0 + (NUM_X+1);
            int i3 = i2 + 1;

            if ((j+i)%2)
            {
                *id++ = i0; *id++ = i2; *id++ = i1;
                *id++ = i1; *id++ = i2; *id++ = i3;
            }
            else
            {
                *id++ = i0; *id++ = i2; *id++ = i3;
                *id++ = i0; *id++ = i3; *id++ = i1;
            }
        }
    }

    GL_CHECK_ERRORS;

    glGenVertexArrays(1,&mVaoID);
    glGenBuffers(1,&mVboVerticesID);
    glGenBuffers(1,&mVboIndicesID);

    glBindVertexArray(mVaoID);

    glBindBuffer(GL_ARRAY_BUFFER,mVboVerticesID);

    //pass the plane vertices to the array buffer object
    glBufferData(GL_ARRAY_BUFFER,sizeof(glm::vec3) * mVertices.size(),&mVertices[0],GL_STATIC_DRAW);

    GL_CHECK_ERRORS;
    glEnableVertexAttribArray((GLuint)0);
    glVertexAttribPointer((GLuint)0,3,GL_FLOAT,GL_FALSE,0,0);

    GL_CHECK_ERRORS;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,mVboIndicesID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(GLushort) * mIndices.size(),&mIndices[0],GL_STATIC_DRAW);

    glDisableVertexAttribArray((GLuint)0);
    glBindVertexArray(0);

    GL_CHECK_ERRORS;

    mInitialized = true;
}

void RippleScene::render()
{
    static const GLfloat color[] = {0.0f,0.0f,0.0f,1.0f};
    static const GLfloat depth = 1.0f;

    glClearBufferfv(GL_COLOR,0,color);
    glClearBufferfv(GL_DEPTH,0,&depth);

    glm::mat4 T = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.0f,mDist));
    glm::mat4 Rx = glm::rotate(T, glm::radians(static_cast<float>(mRx)),glm::vec3(1.0f,0.0f,0.0f));

    glm::mat4 MV = glm::rotate(Rx,glm::radians(static_cast<float>(mRy)),glm::vec3(0.0f,1.0f,0.0f));

    //calculate the model-view matrix
    mModelViewMatrix = mProjectionMatrix * MV;

    //build the shader
    mRippleShader->Use();

    glBindVertexArray(mVaoID);
    glEnableVertexAttribArray(0);

    glUniformMatrix4fv(mRippleShader->getUniform("MVP"),1,GL_FALSE,glm::value_ptr(mModelViewMatrix));
    glUniform1f(mRippleShader->getUniform("time"),mCurrentTime);

    //draw the mesh triangles
    glDrawElements(GL_TRIANGLES,TOTAL_INDICES,GL_UNSIGNED_SHORT,0);

    glBindVertexArray(0);
    glDisableVertexAttribArray(0);

    mRippleShader->UnUse();

}

void RippleScene::update(float t)
{
    mCurrentTime = t * SPEED;
}

void RippleScene::loadShaders()
{

    mRippleShader = new GLSLShader();

    mRippleShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/ripple.vert");
    mRippleShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/ripple.frag");
    mRippleShader->CreateAndLinkProgram();

    mRippleShader->Use();

    mRippleShader->AddUniform("MVP");
    mRippleShader->AddUniform("time");

    mRippleShader->UnUse();
}

void RippleScene::paint()
{
    if(!mInitialized)
        initialise();

    resize(mWidth,mHeight);

    render();
}

void RippleScene::setViewportSize(int width, int height)
{
    mWidth  = width;
    mHeight = height;
}

void RippleScene::resize(int w, int h)
{
    if(h < 1)
        h = 1;

    mWidth = w;
    mHeight = h;

    glViewport(0,0,GLsizei(mWidth),GLsizei(mHeight));

    mProjectionMatrix = glm::perspective(glm::radians(45.0f),(GLfloat)mWidth/mHeight,1.0f,1000.0f);
}

double RippleScene::rX() const
{
    return static_cast<double>(mRx);
}

double RippleScene::rY() const
{
    return static_cast<double>(mRy);
}


double RippleScene::distance() const
{
    return static_cast<double>(mDist);
}


void RippleScene::setDistance(double distance)
{
    mDist = static_cast<float>(distance);
}

void RippleScene::setRX(double rx)
{
    mRx = static_cast<float>(rx);
}

void RippleScene::setRY(double ry)
{
    mRy = static_cast<float>(ry);
}


