QT += core gui qml quick widgets
CONFIG += c++11

HEADERS += $$PWD/AbstractScene.h \
           $$PWD/defines.h \
           $$PWD/GLSLShader.h

SOURCES += $$PWD/AbstractScene.cpp \
           $$PWD/GLSLShader.cpp
