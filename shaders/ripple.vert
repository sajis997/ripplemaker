#version 430 compatibility

layout (location = 0) in vec3 vVertex;

//combined model-view projection matrix
uniform mat4 MVP;
uniform float time;

//shader constants
const float amplitude = 0.125;
const float frequency = 4;
const float PI = 3.14159;

void main()
{
    //get the euclidian distance of the current vertex from the center of the mesh
    float distance = length(vVertex);

    //create a sin function using the distance,
    //multiply the frequency and add the elapsed time
    float y = amplitude * sin(-PI * distance * frequency + time * 2);

    //multiply the MVP matrix with new position to get the clipspace position
    //output position of the vertex
    gl_Position = MVP * vec4(vVertex.x,y,vVertex.z,1);
}
