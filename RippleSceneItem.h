#ifndef RIPPLESCENEITEM_H
#define RIPPLESCENEITEM_H

#include <QTime>
#include <QtQuick/QQuickItem>

class QTimer;
class RippleScene;

class RippleSceneItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(double rX READ rX WRITE setRX NOTIFY rXChanged)
    Q_PROPERTY(double rY READ rY WRITE setRY NOTIFY rYChanged)

    Q_PROPERTY(double distance READ distance WRITE setDistance NOTIFY distanceChanged)

public:
    RippleSceneItem();

    double rX() const;
    double rY() const;
    double distance() const;


    void setDistance(double);
    void setRX(double);
    void setRY(double);


signals:

    void rXChanged();
    void rYChanged();

    void distanceChanged();

public slots:

    void updateTime();
    void sync();
    void cleanup();

private slots:
    void handleWindowChanged(QQuickWindow*);

private:
    RippleScene *mRippleScene;

    QTime mTime;

    QTimer *mTimer;
};

#endif // RIPPLESCENEITEM_H
