#ifndef RIPPLESCENE_H
#define RIPPLESCENE_H

#include <QOpenGLFunctions_4_3_Compatibility>

#include "defines.h"
#include "AbstractScene.h"

class GLSLShader;

class RippleScene : public AbstractScene , protected QOpenGLFunctions_4_3_Compatibility
{
    Q_OBJECT

public:
    explicit RippleScene(QObject *parent = 0);
    ~RippleScene();

    //scene related functions
    virtual void initialise();
    virtual void update(float t);
    virtual void render();
    virtual void resize(int w,int h);

    void setViewportSize(int width,int height);

    double rX() const;
    double rY() const;
    double distance() const;

    void setRX(double);
    void setRY(double);
    void setDistance(double);

public slots:
    void paint();

private:
    void loadShaders();

    GLSLShader *mRippleShader;

    GLuint mVaoID;
    GLuint mVboVerticesID;
    GLuint mVboIndicesID;

    const int NUM_X;
    const int NUM_Z;

    const float SIZE_X;
    const float SIZE_Z;

    const float HALF_SIZE_X;
    const float HALF_SIZE_Z;

    const int TOTAL_INDICES;

    const float SPEED;

    //ripple mesh vertices and indices
    std::vector<glm::vec3> mVertices;
    std::vector<GLushort> mIndices;

    glm::mat4 mProjectionMatrix;
    glm::mat4 mModelViewMatrix;

    int mWidth;
    int mHeight;

    bool mInitialized;

    float mCurrentTime;

    float mDist;

    double mRx, mRy;
};


#endif // RIPPLESCENE_H
