#include <QGuiApplication>
#include <QtQuick/QQuickView>

#include "RippleSceneItem.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc,argv);

    qmlRegisterType<RippleSceneItem>("RippleScene",1,0,"RippleSceneItem");

    QQuickView view;

    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl("qrc:///qml/main.qml"));
    view.show();

    return app.exec();
}
